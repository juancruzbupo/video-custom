import { all, takeLatest, put } from 'redux-saga/effects'
import { ADD_CLIP_ACTION } from './constants'
import { setClipAction } from './accions'

function* setSaga(action) {
    try {

        yield put(setClipAction(action.payload))

    } catch (err) {
        console.log(err)
    }
}

function* watchAddAction() {
    yield takeLatest(ADD_CLIP_ACTION, setSaga)
}

export default function* step1Saga() {
    yield all([
        watchAddAction(),
    ])
}
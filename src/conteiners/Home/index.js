import React, { Component } from 'react'
import { withRouter, Link  } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Grid, Button, List, Form } from 'semantic-ui-react'
import { Player, ControlBar } from 'video-react'
import { cleanVideoAction } from '../LoadVideo/accions'
import { deleteClipAction } from '../LoadClip/accions'

import DownloadButton from './downloadButton';

export class componentName extends Component {
    state = {
        video: '',
        clips: [],
        startTime: 0,
        endTime: null,
        maxTime: 99999,
        nextPlayer: null,
        prevPlater: null,
        player: {}
    }

    static propTypes = {
        handleCleanVideo: PropTypes.func,
        handleDeleteClip: PropTypes.func,
        urlVideo: PropTypes.string,
        clips: PropTypes.array
    }

    componentWillMount() {
        const {
            urlVideo,
            clips
        } = this.props;

        if (urlVideo !== '') {
            this.setState({
                video: urlVideo
            })
        }

        if (clips.length > 0) {
            this.setState({
                clips,
                nextPlayer: 0
            })
        }
    }

    componentDidMount() {
        // subscribe state change
        this.refs.player.subscribeToStateChange(this.handleStateChange);
    }

    componentWillReceiveProps(nextProps) {
        const {
            urlVideo,
            clips
        } = nextProps;
        
        if (urlVideo === '') {
            this.setState({
                video: ''
            })
        }         
        
        this.setState({
            clips,
            startTime: 0,
            endTime: null,
            nextPlayer: 0
        })
        
    }

    handleStateChange = (state, prevState) => {
        // copy player state to this component's state
        this.setState({
            player: state,
            maxTime: state.duration
        });
    }

    handleLoadClip = () => {
        const { maxTime } = this.state
        const { history } = this.props

        history.push(`/loadclip/add/${maxTime.toFixed(0)}`)
    }

    handleDelete = () => this.props.handleCleanVideo()

    handleDeleteClip = (index) => (e) => this.props.handleDeleteClip(index)

    handleEditClip = (index) => (e) => {
        const { maxTime } = this.state
        const { history } = this.props

        history.push(`/loadclip/edit/${maxTime.toFixed(0)}/${index}`)
    }

    handleClipVideo = () => {
        const { clips } = this.state 
        let next = null

        if(clips.length > 0) {
            next = 0
        }

        this.setState({
            startTime: 0,
            endTime: null,
            nextPlayer: next
        }, () => {
            this.refs.player.load()
        })
    }

    handleClipItem = (index) => (e) => {
        const { clips } = this.state
        const lastIndex = clips.length - 1
        let next = null 

        if (index < lastIndex) {
            next = index + 1
        }

        this.setState({
            startTime: clips[index].start,
            endTime: clips[index].end,
            prevPlater: index,
            nextPlayer: next
        }, () => {
            this.refs.player.load()
        })
    }

    stopPlayer = () => {
        this.refs.player.pause()
    }

    play = () => {
        this.refs.player.play()
    }

    pause = () => {
        this.refs.player.pause()
    }

    fullScreen = () => {
        this.refs.player.toggleFullscreen()
    }

    handleChangeTimeline = () => (e) => {
        this.refs.player.seek(e.target.value)
    }

    handleBackward = () => {
        const { prevPlater } = this.state
        
        if (prevPlater !== null) {
            if (prevPlater > 0) {
                const index = parseInt(prevPlater) - 1
                const { clips } = this.state

                this.setState({
                    startTime: clips[index].start,
                    endTime: clips[index].end,
                    prevPlater: index,
                    nextPlayer: index + 1
                }, () => {
                    this.refs.player.load()
                })
            } else {
                this.setState({
                    startTime: 0,
                    endTime: null,
                    prevPlater: null,
                    nextPlayer: 0
                }, () => {
                    this.refs.player.load()
                })
            }
        }
         
    }

    handleForward = () => {
        const { nextPlayer, clips } = this.state

        if (nextPlayer !== null && nextPlayer < clips.length) {

            this.setState({
                startTime: clips[nextPlayer].start,
                endTime: clips[nextPlayer].end,
                prevPlater: nextPlayer,
                nextPlayer: nextPlayer + 1
            }, () => {
                this.refs.player.load()
            })
        }
    }

    render() {
        const {
            video,
            clips,
            startTime,
            maxTime,
            endTime,
            player
        } = this.state

        let endTimeLine = maxTime

        if (endTime) {
            endTimeLine = endTime
            if (endTime === player.currentTime.toFixed(0)) {
                this.stopPlayer()
            }            
        }   

        return (
            <Grid>
                <Grid.Row>
                    <Grid.Column width={4}>
                        { video !== '' ? (
                            <div>
                                <List divided relaxed>
                                    <List.Item onClick={this.handleClipVideo}>
                                        <List.Icon name='video camera' size='large' verticalAlign='middle' />
                                        <List.Content>
                                            <List.Header as='a'>Full video <Button floated='right' size='mini' icon='trash' onClick={this.handleDelete}/></List.Header>
                                        </List.Content>
                                    </List.Item>
                                    {
                                        clips.map((clip, index) => 
                                            <List.Item key={index} onClick={this.handleClipItem(index)}>
                                                <List.Icon name='video camera' size='large' verticalAlign='middle' />
                                                <List.Content>
                                                    <List.Header as='a'>{clip.name}</List.Header>
                                                    <List.Description as='a'>start time: {clip.start} - end time {clip.end}</List.Description>
                                                    <Button floated='left' size='mini' icon='edit' onClick={this.handleEditClip(index)}/>
                                                    <Button floated='left' size='mini' icon='trash' onClick={this.handleDeleteClip(index)}/>
                                                </List.Content>
                                            </List.Item>
                                        )
                                    }
                                </List>
                                
                                <Button positive onClick={this.handleLoadClip}>Add Clip</Button>
                                
                            </div>
                        ) : (
                            <Link to='/loadvideo'>
                                <Button positive>Load Video</Button>
                            </Link>                            
                        )
                        }
                        
                    </Grid.Column>
                    <Grid.Column width={10}>
                        <Form>
                            <Player
                                ref='player'
                                autoPlay
                                startTime={parseInt(startTime)}
                                >
                                <source src={video} />
                                <ControlBar autoHide={false} disableDefaultControls={true}>
                                    <DownloadButton/>
                                </ControlBar>
                            </Player>
                            {
                                video !== '' ? (
                                    <div>
                                        <Form.Input
                                            min={startTime}
                                            max={endTimeLine}
                                            name='duration'
                                            onChange={this.handleChangeTimeline(this)}
                                            step={1}
                                            type='range'
                                            value={player.currentTime}
                                        />
                                        <Button icon='play' onClick={this.play}/>
                                        <Button icon='pause' onClick={this.pause}/>
                                        <Button icon='expand' onClick={this.fullScreen}/>
                                        <Button icon='backward' onClick={this.handleBackward}/>
                                        <Button icon='forward' onClick={this.handleForward}/>
                                    </div>
                                ) : (
                                    <span />
                                )
                            }
                        </Form>
                    </Grid.Column>
                </Grid.Row> 
            </Grid>
        )
    }
}

const mapStateToProps = (state) => ({
    urlVideo: state.loadVideoReducer.urlVideo,
    clips: state.loadClipReducer.clips
})

const mapDispatchToProps = (dispatch) => ({
  handleCleanVideo: () => dispatch(cleanVideoAction()),
  handleDeleteClip: (index) => dispatch(deleteClipAction(index)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(componentName))

import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import loadVideoReducer from './conteiners/LoadVideo/reducer'
import loadClipReducer from './conteiners/LoadClip/reducer'

const rootReducer = combineReducers({
    loadVideoReducer,
    loadClipReducer,
    routing: routerReducer
});

export default rootReducer

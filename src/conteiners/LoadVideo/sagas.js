import { all, takeLatest, put } from 'redux-saga/effects'
import { ADD_VIDEO_ACTION, CLEAN_VIDEO_ACTION } from './constants'
import { setVideoAction, deleteVideoAction } from './accions'
import { cleanClipAction } from '../LoadClip/accions'

function* setSaga(action) {
    try {

        yield put(setVideoAction(action.payload))

        yield put(cleanClipAction())

    } catch (err) {
        console.log(err)
    }
}

function* deleteSaga() {
    try {

        yield put(deleteVideoAction());

    } catch (err) {
        console.log(err)
    }
}

function* watchAddAction() {
    yield takeLatest(ADD_VIDEO_ACTION, setSaga)
}

function* watchDeleteAction() {
    yield takeLatest(CLEAN_VIDEO_ACTION, deleteSaga)
}

export default function* step1Saga() {
    yield all([
        watchAddAction(),
        watchDeleteAction()
    ])
}

import { all } from 'redux-saga/effects'
import loadVideoSagas from './conteiners/LoadVideo/sagas'
import loadClipSaga from './conteiners/LoadClip/sagas'

export default function* rootSaga () {
    yield all([
        loadVideoSagas(),
        loadClipSaga()
    ])
}
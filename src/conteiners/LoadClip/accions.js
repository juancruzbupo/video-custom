import {
    ADD_CLIP_ACTION,
    SET_CLIP_ACTION,
    DELETE_CLIP_ACTION,
    CLEAN_CLIP_ACTION,
    EDIT_CLIP_ACTION
} from './constants'

export const addClipAction = (payload) => {
    return {
        type: ADD_CLIP_ACTION,
        payload
    };
}

export const setClipAction = (payload) => {
    return {
        type: SET_CLIP_ACTION,
        payload
    }
}

export const deleteClipAction = (payload) => {
    return {
        type: DELETE_CLIP_ACTION,
        payload
    }
}

export const cleanClipAction = () => {
    return {
        type: CLEAN_CLIP_ACTION
    }
}

export const editClipAction = (payload) => {
    return {
        type: EDIT_CLIP_ACTION,
        payload
    }
}
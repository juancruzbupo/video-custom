import {
    SET_CLIP_ACTION,
    DELETE_CLIP_ACTION,
    CLEAN_CLIP_ACTION,
    EDIT_CLIP_ACTION
} from './constants'

const initialState = { clips: []}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CLIP_ACTION:
            return Object.assign({}, state, {
                ...state,
                clips: [
                    ...state.clips,
                    action.payload
                ]
            })
        case DELETE_CLIP_ACTION:
            return Object.assign({}, state, {
                ...state,
                clips: state.clips.filter((clip, index) => {
                    if (index !== action.payload) {
                        return clip
                    }
                })
            })
        case CLEAN_CLIP_ACTION:
            return Object.assign({}, state, {
                ...state,
                clips: []
            })
        case EDIT_CLIP_ACTION:
            return Object.assign({}, state, {
                ...state,
                clips: state.clips.map((clip, index) => {
                    if (index === parseInt(action.payload.index)) {
                        return Object.assign({}, clip, {
                            name: action.payload.data.name,
                            start: action.payload.data.start,
                            end: action.payload.data.end,
                        })
                    }
                    return clip
                })
            })
        default:
            return state
    }
}

export default reducer
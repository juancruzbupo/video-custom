import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter, Link, Redirect  } from 'react-router-dom'
import { connect } from 'react-redux'
import { Grid, Button, Form, Header } from 'semantic-ui-react'
import { addVideoAction } from './accions'

export class componentName extends Component {
    state = {
        _loadingButton: false,
        redirectTo: false,
        urlVideo: ''
    }
    static propTypes = {
        handleAddVideo: PropTypes.func,
        urlVideo: PropTypes.string
    }

    componentWillReceiveProps(nextProps) {
        const { urlVideo } = nextProps;
        
        if (urlVideo !== '') {
            this.setState({
                redirectTo: true
            })
         }        
    }

    handleSubmit = () => {
        const {
            urlVideo
        } = this.state;

        if (urlVideo !== '') {
            this.props.handleAddVideo(urlVideo);

            this.setState({
                _loadingButton: true
            })

        } else {

        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    render() {
        const {
            _loadingButton,
            redirectTo
        } = this.state

        if (redirectTo) {
            return <Redirect to={{pathname: '/'}} />;
        }
        return (
            <Grid>
                <Grid.Row columns={2}>
                    <Grid.Column>
                        <Header as='h1'>Load Video</Header>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Input
                                label='URL Video'
                                placeholder='URL Video'
                                name='urlVideo'
                                onChange={this.handleChange}
                                type='input'
                                required
                            />
                            <Button type='submit' positive loading={_loadingButton}>Confirm</Button>
                            <Link to='/'>
                                <Button>Back</Button>
                            </Link>
                        </Form>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => ({
    urlVideo: state.loadVideoReducer.urlVideo
})

const mapDispatchToProps = (dispatch) => ({
    handleAddVideo: (payload) => dispatch(addVideoAction(payload))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(componentName))

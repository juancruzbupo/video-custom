import React, { Component } from 'react'
import { Container } from 'semantic-ui-react'
import AppRoutes from './routes'
import './App.css'

import 'semantic-ui-css/semantic.min.css'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <header className='App-header'>
          <h1 className='App-title'>Video Custom</h1>
        </header>
        <br />
        <Container textAlign='left'>
          <AppRoutes />  
        </Container>
      </div>
    )
  }
}

export default App

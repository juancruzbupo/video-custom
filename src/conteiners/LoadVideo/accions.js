import {
    ADD_VIDEO_ACTION,
    SET_VIDEO_ACTION,
    DELETE_VIDEO_ACTION,
    CLEAN_VIDEO_ACTION,
} from './constants'

export const addVideoAction = (payload) => {
    return {
        type: ADD_VIDEO_ACTION,
        payload
    }
}

export const setVideoAction = (payload) => {
    return {
        type: SET_VIDEO_ACTION,
        payload
    }
}

export const cleanVideoAction = (payload) => {
    return {
        type: CLEAN_VIDEO_ACTION
    }
}

export const deleteVideoAction = (payload) => {
    return {
        type: DELETE_VIDEO_ACTION
    }
}
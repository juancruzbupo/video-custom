import React from 'react';
import { Route, Switch } from "react-router"

import Home from './conteiners/Home'
import LoadVideo from './conteiners/LoadVideo'
import LoadClip from './conteiners/LoadClip'

export default () => {
 return (
    <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/loadvideo" component={LoadVideo} />
        <Route path="/loadclip/:action/:duration/:index?" component={LoadClip} />
    </Switch>
    
 )
}
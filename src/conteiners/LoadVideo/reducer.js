import {
    SET_VIDEO_ACTION,
    DELETE_VIDEO_ACTION
} from './constants'

const initialState = { urlVideo: ''}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_VIDEO_ACTION:
            return Object.assign({}, state, {
                ...state,
                urlVideo: action.payload
            })
        case DELETE_VIDEO_ACTION:
            return Object.assign({}, state, {
                ...state,
                urlVideo: ''
            })
        default:
            return state
    }
}

export default reducer
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter, Link, Redirect  } from 'react-router-dom'
import { connect } from 'react-redux'
import { Grid, Button, Form, Header, Label } from 'semantic-ui-react'
import {
  addClipAction,
  editClipAction
} from './accions'

export class LoadClip extends Component {
  state = {
    _loadingButton: false,
    redirectTo: false,
    name: '',
    start: '',
    end: '',
    title: 'Load',
    index: '',
    message: ''
  }

  static propTypes = {
    handleAddClip: PropTypes.func,
    handleEditClip: PropTypes.func,
    clips: PropTypes.array
  }

  componentWillMount(){
    const {
      action,
      index,
      duration
    } = this.props.match.params
    
    if (action === 'edit') {
      const clip = this.props.clips[index]
      this.setState({
        name: clip.name,
        start: clip.start,
        end: clip.end,
        index: index,
        title: 'Edit'

      })
    }

  }

  componentWillReceiveProps(nextProps) {
    const { clips } = nextProps;
    
    if (clips.length > 0) {
        this.setState({
            redirectTo: true
        })
      }        
  }

  handleSubmit = () => {
    const { duration } = this.props.match.params
    const {
      name,
      start,
      end,
      title,
      index
    } = this.state;

    let error = false

    if(!error && parseInt(end) > parseInt(duration)) {
      this.setState({
        message: 'End time cannot be longer than the duration of the video'
      })
      error = true
    }

    if (!error && parseInt(end) <= parseInt(start)) {
      this.setState({
        message: 'End time cannot be less than or equal to start time'
      })
      error = true
    }

    if (!error && parseInt(start) < 0) {
      this.setState({
        message: 'Start time cannot be less than 0'
      })
      error = true
    }

    if (name !== '' && !error) {
      if (title !== 'Edit') {
        this.props.handleAddClip({name, start, end});
      } else {
        this.props.handleEditClip({
          index: index,
          data: {
            name,
            start,
            end
          }
        })
      }
      

      this.setState({
        _loadingButton: true
      })

    }

  }

  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  render() {
    const {
      _loadingButton,
      redirectTo,
      name,
      start,
      end,
      title,
      message
    } = this.state

    if (redirectTo) {
      return <Redirect to={{pathname: '/'}} />;
    }

    const header = `${title} Clip`

    return (
      <Grid>
        <Grid.Row columns={2}>
          <Grid.Column>
              <Header as='h1'>{header}</Header>
              <Form onSubmit={this.handleSubmit}>
                  <Form.Input
                      label='Clip Name'
                      placeholder='Clip Name'
                      name='name'
                      value={name}
                      onChange={this.handleChange}
                      type='input'
                      required
                  />
                  <Form.Input
                      label='Start Time'
                      placeholder='Start Time'
                      name='start'
                      value={start}
                      onChange={this.handleChange}
                      type='input'
                      required
                  />
                  <Form.Input
                      label='End Time'
                      placeholder='End Time'
                      name='end'
                      value={end}
                      onChange={this.handleChange}
                      type='input'
                      required
                  />
                  <Button type='submit' positive loading={_loadingButton}>Confirm</Button>
                  <Link to='/'>
                      <Button>Back</Button>
                  </Link>
              </Form>
              <br />
              {
                message !== '' ? (
                    <Label color='red' key='red'>
                        { message }  
                    </Label>
                ) : (
                    <span />
                )
              }
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => ({
  clips: state.loadClipReducer.clips
})

const mapDispatchToProps = (dispatch) => ({
  handleAddClip: (payload) => dispatch(addClipAction(payload)),
  handleEditClip: (payload) => dispatch(editClipAction(payload))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoadClip))
